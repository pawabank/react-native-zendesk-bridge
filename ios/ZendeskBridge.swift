import ZendeskCoreSDK
import SupportSDK
import MessagingSDK
import ChatSDK
import AnswerBotSDK
import SupportProvidersSDK
import AnswerBotProvidersSDK
import ChatProvidersSDK

@objc(ZendeskBridge)
class ZendeskBridge: NSObject {

    @objc(multiply:withB:withResolver:withRejecter:)
    func multiply(a: Float, b: Float, resolve:RCTPromiseResolveBlock,reject:RCTPromiseRejectBlock) -> Void {
        resolve(a*b)
    }

    @objc(add:withB:withResolver:withRejecter:)
    func add(a: Float, b: Float, resolve:RCTPromiseResolveBlock,reject:RCTPromiseRejectBlock) -> Void {
        resolve(a+b)
    }

    @objc(initialize:withB:withResolver:withRejecter:)
    func initialize(a: Float, b: Float, resolve:RCTPromiseResolveBlock,reject:RCTPromiseRejectBlock) -> Void {
        Zendesk.initialize(appId: "TODO", clientId: "TODO", zendeskUrl: "TODO")
        Support.initialize(withZendesk: Zendesk.instance)
        AnswerBot.initialize(withZendesk: Zendesk.instance, support: Support.instance!)
        Chat.initialize(accountKey: "TODO")
        resolve(true)
    }

    @objc(buildUI:withB:withResolver:withRejecter:)
    func buildUI(a: Float, b: Float, resolve:RCTPromiseResolveBlock,reject:RCTPromiseRejectBlock) -> Void {
        do { try DispatchQueue.main.sync {

            let identity = Identity.createAnonymous(name: "John Bob", email: "johnbob@example.com")
            Zendesk.instance?.setIdentity(identity)

            let messagingConfiguration = MessagingConfiguration()
            let answerBotEngine = try AnswerBotEngine.engine()
            let supportEngine = try SupportEngine.engine()
            let chatEngine = try ChatEngine.engine()

            if var topController = UIApplication.shared.keyWindow?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }

                let controller = try Messaging.instance.buildUI(engines: [answerBotEngine, supportEngine, chatEngine], configs: [messagingConfiguration])
                let navControl = UINavigationController(rootViewController: controller)
                topController.present(navControl, animated:true, completion: nil)
            }

        }} catch {
            reject("event_failure", "no event id returned", nil);
        }
        resolve(true)
    }
}

@objc(AwesomeModuleViewManager)
class AwesomeModuleViewManager: RCTViewManager {


  override func view() -> (UIView) {
    return RequestUi.buildRequestUi(with: []).view

  }
}

class AwesomeModuleView : UIView {

  @objc var color: String = "" {
    didSet {
      self.backgroundColor = hexStringToUIColor(hexColor: color)
    }
  }

  func hexStringToUIColor(hexColor: String) -> UIColor {
    let stringScanner = Scanner(string: hexColor)

    if(hexColor.hasPrefix("#")) {
      stringScanner.scanLocation = 1
    }
    var color: UInt32 = 0
    stringScanner.scanHexInt32(&color)

    let r = CGFloat(Int(color >> 16) & 0x000000FF)
    let g = CGFloat(Int(color >> 8) & 0x000000FF)
    let b = CGFloat(Int(color) & 0x000000FF)

    return UIColor(red: r / 255.0, green: g / 255.0, blue: b / 255.0, alpha: 1)
  }
}
