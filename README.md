# react-native-zendesk-bridge

Bridge for the native Zendesk SDK

## Installation

```sh
npm install react-native-zendesk-bridge
# or
yarn add react-native-zendesk-bridge
```
You also need to add the following to your `android/app/build.gradle`:
```
repositories {
    maven { url 'https://zendesk.jfrog.io/zendesk/repo' }
}

dependencies {
    api group: 'com.zendesk', name: 'messaging', version: '5.2.0'
    api group: 'com.zendesk', name: 'support', version: '5.0.3'
    api group: 'com.zendesk', name: 'answerbot', version: '3.0.2'
    api group: 'com.zendesk', name: 'chat', version: '3.3.0'
}
```
See [Adding the Unified SDK](https://developer.zendesk.com/documentation/classic-web-widget-sdks/unified-sdk/android/sdk_add/) for up-to-date version numbers.

## Usage

```js
import ZendeskBridge from "react-native-zendesk-bridge";

// ...

const result = await ZendeskBridge.multiply(3, 7);
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
