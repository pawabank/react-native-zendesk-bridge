import ZendeskBridge from 'react-native-zendesk-bridge';
import { useEffect } from 'react';

export default function App() {
  const baseUrl = 'https://pawaful.zendesk.com';
  const appId = '0d78e1a1c2e242adacb176cbea1bf3cc1d11ee2a3d27797a';
  const clientId = 'mobile_sdk_client_75b625101cce5a37d80a';
  const chatAccount = 'kRKyRqGI2ISqaOTn9CGAbzOpVjF8IewX';
  const token = 'id-for-jwt-auth';

  useEffect(() => {
    ZendeskBridge.initialize(baseUrl, appId, clientId, chatAccount, token)
      .then(() => ZendeskBridge.buildUI())
      .catch(console.error);
  }, []);

  return null;
}
