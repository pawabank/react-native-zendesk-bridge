import { NativeModules } from 'react-native';

type ZendeskBridgeType = {
  initialize(
    baseUrl: string,
    appId: string,
    clientId: string,
    chatAccount: string,
    token: string
  ): Promise<void>;
  buildUI(): Promise<void>;
};

const { ZendeskBridge } = NativeModules;
export default ZendeskBridge as ZendeskBridgeType;
