package com.reactnativezendeskbridge;

import android.content.Context;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.module.annotations.ReactModule;
import com.zendesk.logger.Logger;

import zendesk.answerbot.AnswerBot;
import zendesk.answerbot.AnswerBotEngine;
import zendesk.chat.Chat;
import zendesk.chat.ChatEngine;
import zendesk.core.Identity;
import zendesk.core.JwtIdentity;
import zendesk.core.Zendesk;
import zendesk.messaging.Engine;
import zendesk.messaging.MessagingActivity;
import zendesk.support.Support;
import zendesk.support.SupportEngine;

@ReactModule(name = ZendeskBridgeModule.NAME)
public class ZendeskBridgeModule extends ReactContextBaseJavaModule {
    public static final String NAME = "ZendeskBridge";

    public ZendeskBridgeModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    @NonNull
    public String getName() {
        return NAME;
    }

    @ReactMethod
    public void initialize(String baseUrl, String appId, String oauthClientId, String chatAccountKey, String token, Promise promise) {
      Logger.setLoggable(true);
      Context context = this.getReactApplicationContext().getCurrentActivity();
      if (context == null) {
        promise.reject("No activity found", "");
        return;
      }
      Zendesk.INSTANCE.init(context, baseUrl, appId, oauthClientId);
      Identity identity = new JwtIdentity(token);
      Zendesk.INSTANCE.setIdentity(identity);
      Support.INSTANCE.init(Zendesk.INSTANCE);
      AnswerBot.INSTANCE.init(Zendesk.INSTANCE, Support.INSTANCE);
      Chat.INSTANCE.init(context, chatAccountKey);
      Chat.INSTANCE.setIdentity(jwtCompletion -> jwtCompletion.onTokenLoaded(token));
      promise.resolve(null);
    }

    @ReactMethod
    public void buildUI(Promise promise) {
      Engine answerBotEngine = AnswerBotEngine.engine();
      Engine supportEngine = SupportEngine.engine();
      Engine chatEngine = ChatEngine.engine();
      Context context = this.getReactApplicationContext().getCurrentActivity();
      if (context == null) {
        promise.reject("No activity found", "");
        return;
      }
      MessagingActivity.builder()
        .withEngines(answerBotEngine, chatEngine, supportEngine)
        .show(context);
      promise.resolve(null);
    }
}
